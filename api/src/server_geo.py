#!/usr/bin/env python3

from flask import Flask, request, jsonify
import os
import json
import datetime
import logging

from sshtunnel import SSHTunnelForwarder

from utils import json_to_dict


def create_ssh_tunnel(
    ssh_host,
    ssh_user,
    ssh_pass,
    remote_ip,
    remote_port
):
    tunnel = SSHTunnelForwarder(
        ssh_host,
        ssh_username=ssh_user,
        ssh_password=ssh_pass,
        remote_bind_address=(remote_ip, remote_port)
    )
    return tunnel


def get_db(credentials, name, ssh=False):
    from pymongo import MongoClient

    if ssh:
        ssh_host     = credentials["host"]
        ssh_user     = credentials["user"]
        ssh_pass     = credentials["pass"]
        remote_ip    = credentials["ip"]
        remote_port  = credentials["port"]

        tunnel = create_ssh_tunnel(
            ssh_host=ssh_host,
            ssh_user=ssh_user,
            ssh_pass=ssh_pass,
            remote_ip=remote_ip,
            remote_port=remote_port
        )

        tunnel.start()

        client = MongoClient(credentials["ip"], tunnel.local_bind_port)  # tunnel.local_bind_port is assigned local port

        db = client[name]
    return db


DATEi = None
PATH = os.path.abspath(os.path.dirname(__file__))

log = logging.getLogger('werkzeug')

app = Flask("Return products from the basket")

CONFIG_PATH = "../config.json"


def read_config(json_file):
    with open(json_file) as f:
        config = json.load(f)
    return config


@app.route('/', methods=['GET'])
def index():
    return "The server is running"


@app.route('/event', methods=['POST'])  # URL: 0.0.0.0:80/status.
def event():

    credentials = json_to_dict(CONFIG_PATH)
    db = get_db(credentials=credentials['mongo'], name='events', ssh=True)

    data = request.json
    temp_list = []

    if ('event_type' in data.keys()) and ('time' in data.keys()):

        if ('begin' in data['time'].keys()) and ('end' in data['time'].keys()):
            data['time']['begin'] = datetime.datetime.strptime(data['time']['begin'], '%m-%d-%Y %H:%M:%S')
            data['time']['end'] = datetime.datetime.strptime(data['time']['end'], '%m-%d-%Y %H:%M:%S')
            res = db.find(
                {
                    "event": data['event_type'],
                    "time.begin": {
                        '$gt': data['time']['begin']
                    },
                    "time.end": {
                        '$lt': data['time']['end']
                    }
                }
            )

        elif ('end' in data['time'].keys()):
            data['time']['end'] = datetime.datetime.strptime(data['time']['end'], '%m-%d-%Y %H:%M:%S')
            res = db.find({"event": data['event_type'], "time.end": {'$lt': data['time']['end']}})

        elif ('begin' in data['time'].keys()):
            data['time']['begin'] = datetime.datetime.strptime(data['time']['begin'], '%m-%d-%Y %H:%M:%S')
            res = db.find({"event": data['event_type'], "time.begin": {'$gt': data['time']['begin']}})

    elif 'event_type' in data.keys():
        res = db.find({"event": data['event_type']})

    elif 'time' in data.keys():
        if ('begin' in data['time'].keys()) and ('end' in data['time'].keys()):
            data['time']['begin'] = datetime.datetime.strptime(data['time']['begin'], '%m-%d-%Y %H:%M:%S')
            data['time']['end'] = datetime.datetime.strptime(data['time']['end'], '%m-%d-%Y %H:%M:%S')
            res = db.find({"time.begin": {'$gt': data['time']['begin']}, "time.end": {'$lt': data['time']['end']}})
        elif ('end' in data['time'].keys()):
            data['time']['end'] = datetime.datetime.strptime(data['time']['end'], '%m-%d-%Y %H:%M:%S')
            res = db.find({"time.end": {'$lt': data['time']['end']}})
        elif ('begin' in data['time'].keys()):
            data['time']['begin'] = datetime.datetime.strptime(data['time']['begin'], '%m-%d-%Y %H:%M:%S')
            res = db.find({"time.begin": {'$gt': data['time']['begin']}})

    if res is None:
        return ''

    for doc in res:
        doc['_id'] = str(doc['_id'])

        if doc['event'] == 'oil_spill':
            doc['event_description'] = "Разлив нефти"
        if doc['event'] == 'person':
            doc['event_description'] = "Обнаружен человек"
        if doc['event'] == 'car':
            doc['event_description'] = "Обнаружена машина"

        doc['frame_opt'] = [str(doc['frame_opt'])]
        doc['frame_IR'] = [str(doc['frame_IR'])]
        doc['time']['begin'] = doc['time']['begin'].strftime('%m-%d-%Y %H:%M:%S')
        doc['time']['end'] = doc['time']['end'].strftime('%m-%d-%Y %H:%M:%S')
        temp_list.append(doc)

    if len(temp_list) == 0:
        return jsonify([])

    temp_list = sorted(temp_list, key=lambda k: k['time']['end'])
    result_list = []
    delta = 3
    # max_dist = 0.6

    for i, element in enumerate(temp_list):
        if i == 0:
            result_list.append(element)
            print(result_list[-1])
            continue
        print(result_list[-1]['time']['begin'] is None)
        print(type(result_list[-1]['time']['begin']))
        if (
            datetime.datetime.strptime(element['time']['end'], '%m-%d-%Y %H:%M:%S') <
            (
                datetime.datetime.strptime(result_list[-1]['time']['begin'], '%m-%d-%Y %H:%M:%S') +
                datetime.timedelta(seconds=int(delta))
            )
        ):
            result_list[-1]['time']['end'] = element['time']['end']
            result_list[-1]['frame_opt'] = result_list[-1]['frame_opt'] + element['frame_opt']
            result_list[-1]['frame_IR'] = result_list[-1]['frame_IR'] + element['frame_IR']

            continue
        result_list.append(element)
    result_final = []
    for element in result_list:
        result_final.append(element)

    return jsonify(result_final)


if __name__ == '__main__':
    app.run(port=80, host="0.0.0.0", debug=True)
