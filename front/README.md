# Front

### Commands

> development `npm run start:dev`

> build `npm run build`

### Docker

> build container `docker build .`

> run container `docker run -p 80:80 [container_id]`