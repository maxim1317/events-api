import React from 'react'

import ReactPaginate from 'react-paginate'

import './styles.css'

const Paginator = props => (
  <ReactPaginate
    previousLabel="Предыдущая страница"
    nextLabel="Следующая страница"
    containerClassName="sphere-paginator"
    { ...props }
    onPageChange={ ({ selected }) => props.onPageChange(selected) }
    forcePage={ props.currentPage }
  />
)

export default Paginator
