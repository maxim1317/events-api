import React, { Component } from 'react'
import PropTypes from 'prop-types'

const ARROW_UP = 38
const ARROW_DOWN = 40
const ARROW_LEFT = 37
const ARROW_RIGHT = 39

const computeItemsToDisplay = (items, currentPage, perPage) => items
  .slice(currentPage * perPage, (currentPage * perPage) + perPage)

export default class PaginatedList extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.any),
    selected: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.shape({}),
    ]),
    compareItems: PropTypes.func,
    onSelect: PropTypes.func,
    itemRenderer: PropTypes.func.isRequired,

    initialPage: PropTypes.number,
    perPage: PropTypes.number,

    Paginator: PropTypes.func.isRequired,
    renderEmpty: PropTypes.func,

    enableKeyboardShortcuts: PropTypes.bool,

    className: PropTypes.string,
  }

  static defaultProps = {
    items: [],
    selected: null,
    compareItems: () => false,
    onSelect: () => {},

    initialPage: 0,
    perPage: 5,

    renderEmpty: () => null,

    enableKeyboardShortcuts: false,

    className: '',
  }


  static getDerivedStateFromProps(props, state) {
    if (props.items.length === 0) {
      return ({
        currentPage: props.initialPage,
        perPage: props.perPage,
      })
    }

    if (props.perPage === state.perPage) return state

    return ({
      currentPage: props.initialPage,
      perPage: props.perPage,
    })
  }

  state = {
    currentPage: this.props.initialPage,
    perPage: this.props.perPage, // eslint-disable-line
  }

  componentDidMount() {
    const { enableKeyboardShortcuts } = this.props

    if (enableKeyboardShortcuts && document) {
      document.addEventListener('keydown', this.handleKeyPress)
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress)
  }

  onPageChange = page => this.setState({ currentPage: page })

  handleKeyPress = (e) => {
    const {
      items,
      perPage,
      initialPage,

      selected,
      compareItems,
      onSelect,
    } = this.props

    const { currentPage } = this.state

    const pageCount = Math.ceil(items.length / perPage)

    if (e.keyCode === ARROW_RIGHT && currentPage < pageCount - 1) {
      this.setState({ currentPage: currentPage + 1 })
    }

    if (e.keyCode === ARROW_LEFT && currentPage > initialPage) {
      this.setState({ currentPage: currentPage - 1 })
    }

    const visibleItems = computeItemsToDisplay(items, currentPage, perPage)
    const isSelectedItemVisible = visibleItems
      .filter(i => compareItems(i, selected))
      .length > 0
    const visibleItemIndex = visibleItems.findIndex(i => compareItems(i, selected))

    if ((e.keyCode === ARROW_DOWN || e.keyCode === ARROW_UP) && !isSelectedItemVisible) {
      onSelect(visibleItems[0])
      e.preventDefault()
      return
    }

    if (e.keyCode === ARROW_DOWN) {
      if (visibleItemIndex < visibleItems.length - 1) {
        onSelect(visibleItems[visibleItemIndex + 1])
        e.preventDefault()
      }
    }

    if (e.keyCode === ARROW_UP) {
      if (visibleItemIndex !== 0) {
        onSelect(visibleItems[visibleItemIndex - 1])
        e.preventDefault()
      }
    }
  }

  render() {
    const {
      items,
      itemRenderer,
      perPage,

      Paginator,
      renderEmpty,

      className,
    } = this.props

    const {
      currentPage,
    } = this.state

    if (!items.length) return renderEmpty()

    return (
      <div className={ className }>
        <div>
          { computeItemsToDisplay(items, currentPage, perPage).map(itemRenderer) }
        </div>
        <Paginator
          pageCount={ Math.ceil(items.length / perPage) }
          currentPage={ currentPage }
          onPageChange={ this.onPageChange }
        />
      </div>
    )
  }
}
