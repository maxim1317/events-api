import React from 'react'

import './styles.css'

const Spinner = () => <div styleName="loader">Loading...</div>

export default Spinner
