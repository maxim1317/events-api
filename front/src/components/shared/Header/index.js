import React from 'react'

import './styles.css'

const Header = () => (
  <header styleName="header">
    <div styleName="logo" />
    <div styleName="slogan">
      <span styleName="text accent">Веб-приложение</span>
      <span styleName="heading ">Для видеоаналитики с дронов</span>
    </div>
  </header>
)

export default Header
