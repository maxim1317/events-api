import React, { Component } from 'react'
import PropTypes from 'prop-types'

import PaginatedList from 'components/shared/PaginatedList'
import Paginator from 'components/shared/Paginator'

import get from 'lodash/get'
import find from 'lodash/find'

import './styles.css'

const PER_PAGE_OPTIONS = [5, 10, 15]

export default class Reports extends Component {
  static propTypes = {
    reports: PropTypes.arrayOf(PropTypes.shape({
      _id: PropTypes.string.isRequired,
      event_description: PropTypes.string,
      geo: PropTypes.object,
      time: PropTypes.object,
    })),
    onSelect: PropTypes.func,
    selected: PropTypes.string,
  }

  static defaultProps = {
    reports: [],
    onSelect: () => { },
    selected: null,
  }

  state = {
    perPage: 5,
  }

  renderReport = report => (
    <div // eslint-disable-line
      key={ `report_${report._id}` }
      styleName={ `report ${this.props.selected === report._id ? 'selected' : ''}` }
      onClick={ () => this.props.onSelect(report._id) }
    >
      <div styleName="report-column extended">
        <div>Начало {report.time.begin}</div>
        <div>Конец {report.time.end}</div>
      </div>
      <div styleName="report-column extended">
        {report.event_description}
      </div>
      <div styleName="report-column">
        <span>lat: {report.geo.latitude}</span>
        <span>lng: {report.geo.longitude}</span>
      </div>
    </div>
  )

  renderPerPageSelect = n => (
    <button key={ `${n}_per-page-select` } styleName="per-page-select" onClick={ () => this.setState({ perPage: n }) }>{n} </button>
  )

  render() {
    const { reports, onSelect, selected } = this.props

    const { perPage } = this.state

    return (
      <section styleName="reports">
        <span>Расширенный отчет</span>
        <div styleName="per-page-container">
          <span>результатов на странице</span>
          { PER_PAGE_OPTIONS.map(this.renderPerPageSelect) }
        </div>
        <PaginatedList
          styleName="reports-container"
          items={ reports }
          itemRenderer={ this.renderReport }
          Paginator={ Paginator }
          enableKeyboardShortcuts

          perPage={ perPage }
          selected={ find(reports, ['_id', selected]) }
          onSelect={ report => onSelect(report._id) }
          compareItems={ (a, b) => get(a, '_id') === get(b, '_id') }
        />
      </section>
    )
  }
}
