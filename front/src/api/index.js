import axios from 'axios'

import set from 'lodash/set'

export const API_TIME_FORMAT = 'MM-dd-yyyy HH:mm:ss'

export const transformBase64ApiImage = string => string.slice(2, string.length - 1)

export const convertDegreesToDecimal = (degrees) => {
  try {
    const [d, min, sec] = degrees.split(';')

    return Number(d) + (Number(min) / 60) + (Number(sec) / 3600)
  } catch (err) {
    return 0
  }
}

export const EVENT_TYPES = {
  PERSON: 'preson',
  CAR: 'car',
  OIL_SPILL: 'oil_spill',
}

const EVENT_ENDPOINT = () => '/api/event'

export default class VisionApi {
  getEvents = ({ start, end, eventType }) => {
    if (!start && !end && !eventType) return Promise.resolve([])

    const params = {}

    if (start) set(params, 'time.begin', start)
    if (end) set(params, 'time.end', end)
    if (eventType) set(params, 'event_type', eventType)

    return axios
      .post(EVENT_ENDPOINT(), params)
      .then(res => res.data)
      .catch(() => [])
  }

  getAllEvents = () => Promise.all([
    this.getEventsByType(EVENT_TYPES.PERSON),
    this.getEventsByType(EVENT_TYPES.CAR),
    this.getEventsByType(EVENT_TYPES.OIL_SPILL),
  ])

  getEvenstByType = type => axios
    .post(EVENT_ENDPOINT(), { event_type: type })
    .then(res => (res.data ? res.data : []))
    .catch(() => [])
}
