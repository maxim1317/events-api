import React from 'react'
import ReactDOM from 'react-dom'

import App from 'containers/App'

import 'react-datepicker/dist/react-datepicker-cssmodules.css'
import './styles/main.css'


ReactDOM.render(
  <App />,
  document.getElementById('react-root'),
)
