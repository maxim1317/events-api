import React, { Component } from 'react'

import { DateTime } from 'luxon'
import ruLocale from 'date-fns/locale/ru'

import find from 'lodash/find'
import get from 'lodash/get'
import zip from 'lodash/zip'

import DatePicker from 'react-datepicker'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import ImageZoom from 'react-medium-image-zoom'
import Select from 'react-select'
import { Carousel } from 'react-responsive-carousel';

import EventsApi, {
  API_TIME_FORMAT,
  transformBase64ApiImage,
  convertDegreesToDecimal,
  EVENT_TYPES,
} from 'api'

import Reports from 'components/Reports'
import Spinner from 'components/shared/Spinner'

import { defaultStyles } from 'constants/styles'

import './styles.css'

const ZOOM_LEVEL = 13

const DATE_FORMAT = 'MMMM d, yyyy HH:mm'
const TIME_FORMAT = 'HH:mm'

const EVENT_TYPE_OPTIONS = [
  { label: 'Человек', value: EVENT_TYPES.PERSON },
  { label: 'Разлив нефти', value: EVENT_TYPES.OIL_SPILL },
  { label: 'Машина', value: EVENT_TYPES.CAR },
]

export default class Display extends Component {
  constructor() {
    super()

    this.eventsApi = new EventsApi()
  }

  state = {
    reports: [],
    isLoadingReports: false,

    selectedReport: null,

    startDate: null,
    endDate: null,
    eventType: null,
  }

  onReportSelect = id => this.setState({ selectedReport: id })


  loadReports = () => {
    this.setState({ isLoadingReports: true, selectedReport: null, reports: [] })

    const { startDate, endDate, eventType } = this.state

    const params = {}

    if (startDate) params.start = DateTime.fromJSDate(startDate).toFormat(API_TIME_FORMAT)
    if (endDate) params.end = DateTime.fromJSDate(endDate).toFormat(API_TIME_FORMAT)
    if (eventType) params.eventType = get(eventType, 'value')

    this.eventsApi
      .getEvents(params)
      .then(reports => this.setState({ reports, isLoadingReports: false, selectedReport: get(reports, '[0]._id') }))
      .catch(() => this.setState({ reports: [], isLoadingReports: false, selectedReport: null }))
  }

  handleChangeEnd = endDate => this.setState({ endDate })
  handleChangeStart = startDate => this.setState({ startDate })
  handleEventTypeChange = eventType => this.setState({ eventType })

  renderOptions = (startDate, endDate, eventType, isLoadingReports) => (
    <div styleName="date-range-container">
      <span styleName="date-range-input-label">Фильтровать события с: </span>
      {
        this.renderDateSelect(
          startDate,
          startDate,
          endDate,
          this.handleChangeStart,
          isLoadingReports,
        )
      }
      <span styleName="date-range-input-label">по: </span>
      {
        this.renderDateSelect(
          endDate,
          startDate,
          endDate,
          this.handleChangeEnd,
          isLoadingReports,
        )
      }
      <span styleName="date-range-input-label">тип события: </span>
      <Select
        styleName="event-type-input"
        placeholder="Выберете тип"
        noOptionsMessage={ () => 'Тип не найден' }
        options={ EVENT_TYPE_OPTIONS }
        isClearable
        isDisabled={ isLoadingReports }
        value={ eventType }
        onChange={ this.handleEventTypeChange }
      />
      {
        isLoadingReports
          ? <Spinner />
          : <div // eslint-disable-line
            styleName="search-button"
            onClick={ this.loadReports }
          />
      }
    </div>
  )

  renderDateSelect = (selected, startDate, endDate, onChange, disabled) => (
    <DatePicker
      styleName="date-range-input"
      selectsEnd
      selected={ selected }
      startDate={ startDate }
      endDate={ endDate }
      onChange={ onChange }
      isClearable={ !disabled }
      dateFormat={ DATE_FORMAT }
      timeFormat={ TIME_FORMAT }
      timeInputLabel="Время: "
      locale={ ruLocale }
      showTimeInput
      disabled={ disabled }
    />
  )

  renderImagesForReport = (id) => {
    if (!id) return <div styleName="report-images">Отчет не выбран</div>

    const { reports } = this.state

    const report = find(reports, ['_id', id])
    const position = [
      convertDegreesToDecimal(report.geo.latitude),
      convertDegreesToDecimal(report.geo.longitude),
    ]

    const opticImages = report.frame_opt.map(frame => ({
      src: `data:image;base64, ${transformBase64ApiImage(frame)} `,
      alt: 'optic image',
      className: 'report-image',
    }))

    const irImages = report.frame_IR.map(frame => ({
      src: `data:image;base64, ${transformBase64ApiImage(frame)} `,
      alt: 'ir image',
      className: 'report-image',
    }))

    const images = zip(opticImages, irImages)

    return (
      <div styleName="report-images">
        {/* <div>Изображения для отчета {report._id}</div> */}
        <div styleName="report-images-container">

          <div className="report-image">
            <Map center={ position } zoom={ ZOOM_LEVEL }>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
              />
              <Marker position={ position }>
                <Popup>{ report.event_description }</Popup>
              </Marker>
            </Map>
          </div>

          <Carousel showIndicators={false} showThumbs={false} showStatus={false} width="20rem">
            {
              images.map(([ opticImage, irImage ], i) => (
                <div key={`image_page_${i}`}>
                  <ImageZoom defaultStyles={ defaultStyles } image={ opticImage } />
                  <ImageZoom defaultStyles={ defaultStyles } image={ irImage } />
                </div>
              ))
            }
          </Carousel>

        </div>
      </div>
    )
  }

  render() {
    const {
      reports,
      selectedReport,
      startDate,
      endDate,
      isLoadingReports,
      eventType,
    } = this.state

    return (
      <section styleName="display">
        <div styleName="images-column">
          {this.renderImagesForReport(selectedReport)}
        </div>
        <div styleName="reports-column">
          {this.renderOptions(startDate, endDate, eventType, isLoadingReports)}
          <Reports
            reports={ reports }
            onSelect={ this.onReportSelect }
            selected={ selectedReport }
          />
        </div>
      </section>
    )
  }
}
